# Desafio-Sicredi

## Dependências - Maven

- O projeto utilizará as seguintes dependências:
    - **Rest-Assured:** 5.4.0
    - **TestNG:** 7.8.0
    - **Jackson-databind:** 2.16.0
    - **Allure TestNG:** 2.25.0


## Configuração e Execução

1. **Configuração do Ambiente:**
    - O Java deverá estar instalado.
    - java version "18.0.2.1 - Oracle Corporation "
    - Apache Maven 3.9.6
    - Allure 2.25.0 (versão mais atual)

2. **Instalação e Configuração do Projeto:**

[![GitLab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=orange)](https://gitlab.com/KelvinBellanBorges/desafio-sicred)
[![Java](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=openjdk&logoColor=black)](https://docs.oracle.com/en/java/javase/17/install/installation-jdk-microsoft-windows-platforms.html#GUID-371F38CC-248F-49EC-BB9C-C37FC89E52A0)
[![Apache Maven](https://img.shields.io/badge/Apache_Maven-3.9.6-0078D6?style=for-the-badge&logo=apache-maven&logoColor=red)](https://maven.apache.org/download.cgi)
[![Allure](https://img.shields.io/badge/Allure_Report-2.25.0-0078D6?style=for-the-badge&logo=allure&logoColor=white)](https://github.com/allure-framework/allure2/releases)
[![IntelliJ IDEA](https://img.shields.io/badge/IntelliJ_IDEA-404040.svg?style=for-the-badge&logo=intellij-idea&logoColor=blue)](https://www.jetbrains.com/idea/)

```bash
git clone https://gitlab.com/KelvinBellanBorges/desafio-sicred.git
```
- Será necessário baixar o maven e o allure acesse os links:
    - Apace Maven
        - _Binary zip archive_
    - Allure Report
    - Após fazer o download, extraia o arquivo, entre na pasta bin, copie a url
        - Então busque por "_Editar as variáveis de ambiente do sistema_"
        - Clique em _Variaveis de ambiente > Variaveis do sistema > path > Novo_ e ensira o seguinte
            - {LOCAL ONDE EXTRAIU OS ARQUIVOS}\allure-2.25.0\allure-2.25.0\bin
            - {LOCAL ONDE EXTRAIU OS ARQUIVOS}\apache-maven-3.9.6-bin\apache-maven-3.9.6\bin


3. **Comando de Execução:**
    - Abra o cmd e encontre o caminho onde salvou o projeto
    - `mvn test`
    - `allure serve allure-results`

## Regras de negócio
| Regra | descrição                                                                                                                                                                                                                                                                                                                          |
  |-------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Total | Indica o número total de resultados disponíveis na busca, independentemente de quantos estão sendo exibidos na resposta atual. É útil para informar o cliente sobre a quantidade total de resultados disponíveis.                                                                                                                  |
| Skip  | Indica quantos resultados devem ser ignorados no início da lista de resultados. Por exemplo, se houver 100 resultados no total e o skip for definido como 10, os primeiros 10 resultados não serão incluídos na resposta                                                                                                           |
| Limit | Indica o número máximo de resultados a serem retornados na resposta atual. Ele limita a quantidade de resultados retornados por página. Se houver 100 resultados no total e o limit for definido como 20, apenas 20 resultados serão incluídos na resposta, começando a partir do valor indicado em "skip", caso esteja definido.  |

## Casos de teste

### Login
| Método | Endpoint          | Descrição                                                                                                                                                          | Critérios de Sucesso         | Ações                                                                                                                                                                                                                                                                                     |
|--------|-------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| POST   | /auth/login       | Realizar uma requisição POST  /auth/login com um conjunto válido de credenciais e verificar se a resposta é um status code 200 OK.                                 | Status code 200 OK.          | - Criar um JSON com um conjunto válido de credenciais. <br> - Realizar a requisição <br/> **POST** - ` /auth/login` <br/> com as credenciais válidas. <br> - Verificar se o status code da resposta é 200. <br> - Validar os dados retornados no corpo da resposta (username, email, id). |
| POST   | /auth/login       | Realizar uma requisição POST  /auth/login com um conjunto de credenciais inválidas (senha incorreta) e verificar se a resposta é um status code 400 Bad Request.   | Status code 400 Bad Request. | - Criar um JSON com um conjunto de credenciais válidas, porém com senha incorreta. <br> - Realizar a requisição <br/> **POST** - `/auth/login` <br/> com as credenciais inválidas. <br> - Verificar se o status code da resposta é 400.                                                   | 
| POST   | /auth/login       | Realizar uma requisição POST  /auth/login com um conjunto de credenciais inválidas (usuário inválido) e verificar se a resposta é um status code 400 Bad Request.  | Status code 400 Bad Request. | - Criar um JSON com um conjunto de credenciais inválidas (usuário inválido). <br> - Realizar a requisição <br/> **POST** - `/auth/login` com as credenciais inválidas. <br> - Verificar se o status code da resposta é 400.                                                               |

### Products
| Método | Endpoint                   | Descrição                                                                                                                                  | Critérios de Sucesso           | Ações                                                                                                                                                                                                                                                                                                                                         |
|--------|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GET    | /auth/products             | Realizar uma requisição GET  /auth/products e verificar se a resposta é um status code 200 OK.                                             | Status code 200 OK.            | - Incluir o token de autorização no cabeçalho. <br> - Definir parâmetros de consulta (total, skip, limit). <br> - Realizar a requisição <br/> **GET** - `/auth/products` <br> - Verificar se o status code da resposta é 200. <br> - Validar o título do primeiro produto retornado. <br> - Verificar o número total de produtos na resposta. |
| GET    | /auth/products             | Realizar uma requisição GET  /auth/products com um token inválido e verificar se a resposta é um status code 401 Unauthorized.             | Status code 401 Unauthorized.  | - Incluir um token inválido no cabeçalho de autorização. <br> - Realizar a requisição <br/> **GET** - `/auth/products` <br> - Verificar se o status code da resposta é 401. <br> - Validar a mensagem de erro retornada.                                                                                                                      |
| GET    | /auth/products             | Realizar uma requisição GET  /auth/products com uma assinatura inválida do token e verificar se a resposta é um status code 403 Forbidden. | Status code 403 Forbidden.     | - Incluir uma assinatura inválida no token de autorização. <br> - Realizar a requisição <br/> **GET** - `/auth/products` <br> - Verificar se o status code da resposta é 403. <br> - Validar a mensagem de erro retornada.                                                                                                                    |
| POST   | /auth/products/add         | Realizar uma requisição POST /auth/products/add com informações válidas do produto e verificar se a resposta é um status code 200 OK.      | Status code 200 OK.            | - Incluir o token de autorização no cabeçalho. <br> - Enviar um payload com informações válidas do produto. <br> - Realizar a requisição <br/> **POST** - `/auth/products/add` <br> - Verificar se o status code da resposta é 200. <br> - Validar o ID do produto retornado.                                                                 |
| POST   | /products                  | Realizar uma requisição POST /products e verificar se a resposta é um status code 404 Not Found.                                           | Status code 404 Not Found.     | - Enviar um payload com informações do produto. <br> - Realizar a requisição <br/> **POST** - `/products` <br> - Verificar se o status code da resposta é 404.                                                                                                                                                                                |
| GET    | /products/{id}             | Realizar uma requisição GET /products/{id} e verificar se a resposta é um status code 200 OK.                                              | Status code 200 OK.            | - Enviar um ID de produto existente. <br> - Realizar a requisição <br/> **GET** - `/products/{id}` <br> - Verificar se o status code da resposta é 200.                                                                                                                                                                                       |
| GET    | /products/{id}             | Realizar uma requisição GET  /products/{id} com um ID de produto não encontrado e verificar se a resposta é um status code 404 Not Found.  | Status code 404 Not Found.     | - Enviar um ID de produto não existente. <br> - Realizar a requisição <br/>  **GET** - `/products/{id}` <br> - Verificar se o status code da resposta é 404. <br> - Validar a mensagem de erro retornada.                                                                                                                                     |

### Serviços
| Método | Endpoint | Descrição                                                                                                                                          | Critérios de Sucesso                           | Ações                                                                                                                                                                             |
|--------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GET    | /test    | Realizar uma requisição GET /test e verificar se a resposta é um status code 200 OK, contendo o status "ok" e o método "GET" no corpo da resposta. | Status code 200 OK, status "ok", método "GET". | - Realizar a requisição <br/> **GET** - `/test` <br> - Verificar se o status code da resposta é 200. <br> - Validar se o corpo da resposta contém o status "ok" e o método "GET". |

### User
| Método | Endpoint           | Descrição                                                                                                                                                          | Critérios de Sucesso                                                    | Ações                                                                                                                                                                                                       |
|--------|--------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GET    | /users             | Realizar uma requisição GET - /users e verificar se a resposta é um status code 200 OK.                                                                            | Status code 200 OK.                                                     | - Realizar a requisição <br/> **GET** - `/users` <br> - Verificar se o status code da resposta é 200.                                                                                                       |
| GET    | /users/inexistente | Realizar uma requisição GET para um usuário inexistente e verificar se a resposta é um status code 400 Bad Request com a mensagem "Invalid user id 'inexistente'". | Status code 400 Bad Request, mensagem "Invalid user id 'inexistente'".  | - Realizar a requisição <br/> **GET** - `/users/inexistente` <br> - Verificar se o status code da resposta é 400. <br> - Validar se o corpo da resposta contém a mensagem "Invalid user id 'inexistente'".  |
| GET    | /use               | Realizar uma requisição GET para um endpoint inexistente e verificar se a resposta é um status code 404 Not Found.                                                 | Status code 404 Not Found.                                              | - Realizar a requisição <br/> **GET** - `/use` <br> - Verificar se o status code da resposta é 404.                                                                                                         |


## Bugs Encontrados

| Bugs                                   | Descrição                                                                                                                                                                                                                                                                                                                                                                                   |
|----------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Cadastro não realizado                 | Após realizar um cadastro com êxito (retorno status 200) <br/> **POST** - `https://dummyjson.com/products/add` <br/> verificou-se a listagem do produto na rota <br/> **GET** - ` https://dummyjson.com/auth/products?total=110&skip=0&limit=101` <br/> Notou-se que o cadastro com ID 101 não está sendo exibido, apesar de ter sido previamente registrado. Pois o limite é de 100 IDs.   |
| Exposição da senha                     | O envio da senha em texto plano no responseBody é um problema de segurança.                                                                                                                                                                                                                                                                                                                 |
| Possível falta de validação de entrada | Não está claro se há validação de entrada adequada para garantir que os dados fornecidos estejam corretos e no formato esperado.                                                                                                                                                                                                                                                            |
| Status 403 Bug                         | Conforme descrito na documentação, a mensagem "invalid signature" deveria aparecer com um status 403, mas atualmente está constando 500 Internal Server Error.                                                                                                                                                                                                                              |

## Melhorias Sugeridas

| Melhorias                                                 | Descrição                                                                                                    |
|-----------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| Exclusão e criptografia do campo password no responseBody | Cada cadastro contém a informação password, este campo deveria ser excluído do responseBody e criptografado. |
| Privacidade dos Dados                                     | Cada cadastro contém a informação password, este campo deveria ser excluído do responseBody e criptografado. |
| Tratativas no token                                       | Há duas tratativas no token (uma quando o token expira) e outra quando é inválida.                           |
