package configuration;
import com.github.javafaker.Faker;


public class Product {

    private static final Faker faker = new Faker();
    public static String createProduct() {
        return String.format("{" +
                        "\n  \"title\": \"%s\",\n  " +
                        "\"description\": \"%s\",\n  " +
                        "\"price\": %d,\n  " +
                        "\"discountPercentage\": %.1f,\n  " +
                        "\"rating\": %.2f,\n  " +
                        "\"stock\": %d,\n  " +
                        "\"brand\": \"%s\",\n  " +
                        "\"category\": \"%s\",\n  " +
                        "\"thumbnail\": \"%s\"\n" +
                        "}",
                faker.commerce().productName(),
                faker.lorem().sentence(),
                faker.number().numberBetween(10, 1000),
                faker.number().randomDouble(1, 0, 50),
                faker.number().randomDouble(2, 0, 5),
                faker.number().numberBetween(0, 100),
                faker.company().name(),
                faker.commerce().department(),
                faker.internet().image());
    }

}