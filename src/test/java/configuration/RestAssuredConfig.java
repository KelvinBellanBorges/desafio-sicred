package configuration;

import tests.login;
import io.restassured.RestAssured;
public class RestAssuredConfig {
    public static void configurationRestAssured() {
        String baseURI = ConfigReader.getProperty("api.key");
        RestAssured.baseURI = baseURI;

    }
}
