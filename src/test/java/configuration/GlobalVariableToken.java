package configuration;

public class GlobalVariableToken {
    private static String authorizationToken;

    public static String getAuthToken() {
        return authorizationToken;
    }

    public static void setAuthToken(String token) {
        authorizationToken = token;
    }

}
