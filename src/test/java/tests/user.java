package tests;

import configuration.RestAssuredConfig;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class user {
    @BeforeClass
    public void configurationBaseUrl(){
        RestAssuredConfig.configurationRestAssured();
    }
    @Test
    public void testUsers() {
        given()
                .when().get("/users")
                .then()
                .statusCode(200)
                .extract().response();
    }

    @Test
    public void testInvalidUser() {
        given()
                .when().get("/users/inexistente")
                .then()
                .statusCode(400)
                .body("message", equalTo("Invalid user id 'inexistente'"))
                .extract().response();
    }
    @Test
    public void testResourceNotFound() {
        given()
                .when().get("/use")
                .then()
                .statusCode(404)
                .extract().response();
    }
}