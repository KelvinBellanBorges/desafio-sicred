package tests;

import configuration.GlobalVariableToken;
import configuration.Product;
import configuration.RestAssuredConfig;
import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;

public class products {

    @BeforeClass
    public void configurationBaseUrl() {
        RestAssuredConfig.configurationRestAssured();
    }

    @Test
    public void testAuthProduct() {
        String authToken = GlobalVariableToken.getAuthToken();
        Response response = given()
                .header("Authorization", "Bearer " + authToken)
                .queryParam("total", 110)
                .queryParam("skip", 0)
                .queryParam("limit", 102)
                .when()
                .get("/auth/products")
                .then()
                .statusCode(200)
                .extract().response();
        String firstProductTitle = response.path("products[0].title");
        assertEquals(firstProductTitle, "iPhone 9", "O título do primeiro produto não é o esperado");
        Allure.step("Validação do primeiro produto");
        int totalProducts = response.path("total");
        assertEquals(totalProducts, 100, "O número total de produtos não é o esperado");
    }

    @Test
    public void testInvalidadeTokenProduct() {
        Response response = given()
                .header("Authorization", "Bearer testInválido2023")
                .when()
                .get("/auth/products")
                .then()
                .statusCode(401)
                .extract().response();
        response.then()
                .body("name", equalTo("JsonWebTokenError"))
                .body("message", equalTo("Invalid/Expired Token!"));
        Allure.step("Validação do token expirado");
    }

    @Test
    public void testInvalidadeSignatureTokenProduct() {
        String authToken = GlobalVariableToken.getAuthToken();
        Response response = given()
                .header("Authorization", "Bearer " + authToken + "1")
                .when()
                .get("/auth/products")
                .then()
                .statusCode(403)
                .extract().response();
        response.then()
                .body("message", equalTo("invalid signature"));
        Allure.step("Validação se o token é inválido");
    }


    @Test
    public void testProductsAdd() {
        String authToken = GlobalVariableToken.getAuthToken();
        Response response = given()
                .header("Authorization", "Bearer " + authToken)
                .body(Product.createProduct())
                .when()
                .post("/auth/products/add")
                .then()
                .statusCode(200)
                .extract().response();
        int id = response.path("id");
        Allure.step("Validação do id de cadastro");
        Assert.assertEquals(101, id);
    }

    @Test
    public void testProduct2() {
        given()
                .body(Product.createProduct())
                .when()
                .post("/products")
                .then()
                .statusCode(404)
                .extract().response();
    }

    @Test
    public void testProductsId() {
        given()
                .body(Product.createProduct())
                .when()
                .get("/products/1")
                .then()
                .statusCode(200)
                .extract().response();
    }

    @Test
    public void testProductsIdNotFound() {
        Response response = given()
                .body(Product.createProduct())
                .when()
                .get("/products/0")
                .then()
                .statusCode(404)
                .extract().response();
        String messageIdNotFound = response.path("message");
        assertEquals(messageIdNotFound, "Product with id '0' not found");
        Allure.step("Validação do Id inexistente");
    }

}
