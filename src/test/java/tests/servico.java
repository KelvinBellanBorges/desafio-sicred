package tests;

import configuration.RestAssuredConfig;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class servico {

    @BeforeClass
    public void configurationBaseUrl(){
        RestAssuredConfig.configurationRestAssured();
    }
    @Test
    public void testTest() {
        given()
                .when().get("/test")
                .then()
                .statusCode(200)
                .body("status", equalTo("ok"))
                .body("method", equalTo("GET"))
                .log().all();
    }
}