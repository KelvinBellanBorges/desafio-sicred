package tests;

import configuration.ConfigReader;
import configuration.GlobalVariableToken;
import configuration.RestAssuredConfig;
import io.qameta.allure.Allure;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;


public class login {
    @BeforeClass
    public void setupRestAssured() {
        RestAssuredConfig.configurationRestAssured();
    }
    @Test

    public void testLoginSuccess() {
        String userJson = createLoginJson(ConfigReader.getProperty("db.username"), ConfigReader.getProperty("db.password"));
        Response response = given()
                .contentType("application/json")
                .body(userJson)
                .when()
                .post("/auth/login")
                .then()
                .statusCode(200)
                .extract().response();

        String authorizationToken = response.path("token");
        String username = response.path("username");
        String email = response.path("email");
        Integer id = response.path("id");

        Assert.assertEquals(ConfigReader.getProperty("db.username"), username);
        Allure.step("Validação do username");
        Assert.assertEquals(ConfigReader.getProperty("db.email"), email);
        Allure.step("Validação do Email");
        Assert.assertEquals(Integer.parseInt(ConfigReader.getProperty("db.id")), id);
        Allure.step("Validação do id");
        GlobalVariableToken.setAuthToken(authorizationToken);
    }

    @Test
    public void testLoginWithIncorrectPassword() {
        String userJson = createLoginJson("kminchelle", "testInválido2023@");
        given()
                .contentType("application/json")
                .body(userJson)
                .when()
                .post("/auth/login")
                .then()
                .statusCode(400)
                .log().all();
    }

    @Test
    public void testLoginIncorrect() {
        String userJson = createLoginJson("testInválido2023@", "testInválido2023");
        given()
                .contentType("application/json")
                .body(userJson)
                .when()
                .post("/auth/login")
                .then()
                .statusCode(400)
                .log().all();
    }

    private String createLoginJson(String username, String password) {
        return String.format("{" +
                "\n  \"username\": \"%s\",\n  " +
                "\"password\": \"%s\"\n" +
                "}", username, password);
    }

}
